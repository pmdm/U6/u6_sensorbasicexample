# TESTING SENSORS 

### Objective

- Test all sensors available on your device/AVD

    **AVD**: use *Virtual Sensors* to simulate device Rotatation/ Movement and Additional Sensors

### Instructions

- Download/Clone repository and deploy on device / AVD
- Activate *Listening?* to get sensor data.
- List: shows device sensor name and information.
- Click on one sensor to see *current* values.


![AVD sensor example](/images/AVD_SensorSimulator.jpg)*Showing current Humidity Sensor values on AVD*
