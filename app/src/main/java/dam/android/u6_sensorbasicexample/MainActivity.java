package dam.android.u6_sensorbasicexample;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private ListView lvSensorList;
    private List<Sensor> mySensorList;
    private TextView tvSensorValues;
    private Switch swStartStop;
    SensorManager sensorManager;
    Sensor currentSensorListening = null;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // get all sensors for device
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mySensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);

        setUI();
    }


    private void setUI() {
        // switch to allow or not listening to sensors
        swStartStop = findViewById(R.id.swStartStop);
        swStartStop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked)
                    stopSensorListener();
            }
        });

        // listview
        lvSensorList = findViewById(R.id.lvSensorList);
        lvSensorList.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, mySensorList));

        lvSensorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                if (swStartStop.isChecked()) {
                    // stop all listeners
                    stopSensorListener();

                    Sensor mySelectedSensor = mySensorList.get(position);

                    if (registerSensorListener(mySelectedSensor))
                        currentSensorListening = mySelectedSensor;
                    else
                        Toast.makeText(getApplicationContext(), "No sensor available for " + mySelectedSensor.getName(), Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(getApplicationContext(), "First, Activate Listening", Toast.LENGTH_LONG).show();
            }
        });

        // values provided for actual sensor
        tvSensorValues = findViewById(R.id.tvSensorValues);
        tvSensorValues.setMovementMethod(new ScrollingMovementMethod());   // allow user to scroll, just in case info from sensor is so long
    }

    private boolean registerSensorListener(Sensor sensor) {

        if (sensor != null)
            return sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        else
            return false;

    }

    private void stopSensorListener() {
        // unregister listener (this activity) for all sensors
        sensorManager.unregisterListener(this);
    }


    @Override
    protected void onPause() {
        super.onPause();

        // always stop listening
        if (swStartStop.isChecked()) {
            swStartStop.setChecked(false);
            tvSensorValues.setText("");
            stopSensorListener();
            Toast.makeText(getApplicationContext(), "Sensor Listening Stopped", Toast.LENGTH_LONG).show();
        }
    }

    // callback methods to manage sensor events
    @Override
    public void onSensorChanged(SensorEvent event) {
        // if we were listening many sensors at a time, every sensor (thread) calls this method
        // so we should sync tv
        synchronized (this) {
            //tvSensorValues.setText("\nSENSOR: " + event.sensor.getName() + "\n VALUES: " + Arrays.toString(event.values));
            tvSensorValues.setText(getString(R.string.sensor_change_show_text, event.sensor.getName(), Arrays.toString(event.values)));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // doing nothing on change of accuracy
    }
}
